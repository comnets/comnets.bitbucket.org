# Converting notebooks to slides
In order to convert to slides, use:

```
jupyter nbconvert docker3-lecture.ipynb \
    --to slides \
    --reveal-prefix ../../reveal.js \
    && mv docker3-lecture.slides.html index.html
```

The flag --reveal-prefix should point to the location of the reveal.js folder

For debugging your slides locally, use the following command

```
jupyter nbconvert docker2-lecture.ipynb \
    --to slides \
    --reveal-prefix ../../reveal.js \
    --post serve
```
